#!/usr/bin/env python3
"""
Listens on an email address for messages from specific users and responds to commands.
"""
import os
import logging
import re
import time
import imaplib
import email
import smtplib
from getpass import getuser
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email import policy
import email.utils

logging.basicConfig(format='[%(asctime)s] %(name)s %(levelname)s %(message)s', level=logging.DEBUG)
logger = logging.getLogger(__name__)

def resolve_value(s):
    if isinstance(s, str) and s.startswith('file://'):
        path = s[7:]
        with open(os.path.expanduser(path)) as fin:
            s = fin.read()
    elif isinstance(s, str) and s.startswith('env://'):
        name = s[6:]
        s = os.environ.get(name)
    return s

class EmailBot:

    def __init__(self):
        try:
            import yaml
            with open('config.yml') as fin:
                self.config = yaml.safe_load(fin.read())
            self.username = resolve_value(self.config['username'])
            assert self.username, 'A username was not specified.'
            self.password = resolve_value(self.config['password'])
            assert self.password, 'A password was not specified.'

            self.callbacks = []
            for pattern_str, exec_str in self.config['callbacks'].items():
                pattern = re.compile(pattern_str, flags=re.I)
                self.callbacks.append((pattern, exec_str))

            self.imap_host = 'imap.gmail.com'

            self.smtp_server = resolve_value(self.config.get('smtp_server', 'smtp.gmail.com'))
            self.smtp_port = int(resolve_value(self.config.get('smtp_port', 587)))

            self.email_whitelist = set(self.config['user_whitelist'])

            self.connect()
        except ImportError:
            logger.warning('PyYAML not installed.')

    def connect(self):
        """
        Connects to the IMAP server.
        """
        logger.info('Connecting.')
        self.mail = imaplib.IMAP4_SSL(self.imap_host, 993)
        rc, resp = self.mail.login(self.username, self.password)

    def check(self):
        """
        Reads the inbox and yields (from, subject) for every unread message that passes the whitelist.
        """
        # select only unread messages from inbox
        logger.info('Looking for new messages.')
        while 1:
            try:
                self.mail.select('Inbox')
                status, data = self.mail.search(None, '(UNSEEN)')
                break
            except imaplib.IMAP4.abort as exc:
                logger.warning('Lost connection to IMAP server: %s', exc)
                self.connect()
                time.sleep(5)

        # for each e-mail messages, print text content
        for num in data[0].split():
            # get a single message and parse it by policy.SMTP (RFC compliant)
            status, data = self.mail.fetch(num, '(RFC822)')
            email_msg = data[0][1]
            email_msg = email.message_from_bytes(email_msg, policy=policy.SMTP)
            message_id = email_msg.get('Message-ID')
            logger.info('Message %s from %s: %s', message_id, email_msg['From'], email_msg['Subject'])
            from_addr = email_msg['From'].addresses[0].addr_spec
            if from_addr not in self.email_whitelist:
                logger.info('Ignoring message from non-whitelisted user %s.', email_msg['From'])
                continue
            yield message_id, email_msg['From'], email_msg['Subject']

    def find_callback(self, subject):
        """
        Looks for a callback matching the subject.
        """
        for pattern, command_base in self.callbacks:
            matches = pattern.findall(subject)
            if matches:
                command = command_base.format(*matches)
                logger.info('Found match: %s', command)
                return command

    def run(self, command):
        """
        Executes the command.
        """
        from subprocess import getoutput
        logger.info('Running: %s', command)
        out = getoutput(command)
        logger.info(out)
        return out

    def send(self, message, recipient, subject, original_id=None):
        """
        Sends an email.
        """

        logger.info('Sending email.')
        myid = email.utils.make_msgid()

        # Initialize mail server connection.
        server = smtplib.SMTP(self.smtp_server, self.smtp_port)
        server.ehlo()
        server.starttls()
        server.ehlo()

        # Compose email.
        msgRoot = MIMEMultipart('related')
        msgRoot['Subject'] = subject
        msgRoot['From'] = self.username
        msgRoot['To'] = recipient
        msgRoot.add_header("Message-ID", myid)
        msgRoot.preamble = msgRoot['Subject']

        if original_id:
            msgRoot.add_header("In-Reply-To", original_id)
            msgRoot.add_header("References", original_id)

        # Construct plain text body.
        msgAlternative = MIMEMultipart('alternative')
        msgRoot.attach(msgAlternative)
        msgAlternative.attach(MIMEText(message))

        # Send.
        server.login(self.username, self.password)
        server.sendmail(msgRoot['From'], msgRoot['To'], msgRoot.as_string())
        server.quit()

        logger.info('Email sent!')

    def action_serve(self):
        """
        Main command loop.
        """
        while 1:
            try:
                for message_id, recipient, subject in self.check():
                    logger.info('Processing command "%s" from %s.', subject, recipient)
                    command = self.find_callback(subject)
                    if command:
                        message = self.run(command)
                    else:
                        message = 'Sorry, but I did not understand.'
                    self.send(message=message, recipient=recipient, subject='RE: %s' % subject, original_id=message_id)
            except Exception as exc:
                logger.exception('Unexpected error: %s', exc)
            time.sleep(15)

    def action_test_email(self):
        """
        Sends a test email to the whitelist.
        """
        self.send('Test', ','.join(sorted(self.email_whitelist)), 'Test')

    def action_install(self, python_version, venv_dir, supervisor_dir, log_path, dryrun=False):
        """
        Installs the script.
        """
        def run(cmd):
            logger.info(cmd)
            if not dryrun:
                os.system(cmd)

        env_path = os.path.join(venv_dir, '.env')
        run(f'[ ! -d .env ] && python{python_version} -m venv {env_path}')
        run(f'{env_path}/bin/pip install -U pip')
        run(f'{env_path}/bin/pip install -r requirements.txt -r requirements-dev.txt')

        user = getuser()
        run(f'sudo touch {log_path}')
        run(f'sudo chown {user}:{user} {log_path}')

        bash_aliases_path = f'/home/{user}/.bash_aliases'
        command_prefix = ''
        if os.path.isfile(bash_aliases_path):
            command_prefix = '. %s; ' % bash_aliases_path

        base_dir = os.path.abspath(os.path.dirname(__file__))
        supervisor_fn = os.path.join(supervisor_dir, 'emailbot.conf')
        conf = f'''
[program:emailbot]
command=bash -c "{command_prefix}{base_dir}/{env_path}/bin/python emailbot.py serve"
directory={base_dir}
user={user}
numprocs=1
process_name=%(program_name)s
stdout_logfile={log_path}
stderr_logfile={log_path}
autostart=true
autorestart=true
startsecs=10
stopasgroup=true
stopsignal=QUIT
        '''
        run(f'sudo tee {supervisor_fn} > /dev/null <<EOF\n{conf}\nEOF')
        run('sudo supervisorctl reload emailbot')

if __name__ == '__main__':

    import argparse

    top_parser = argparse.ArgumentParser(description='Executes commands in response to receiving emails.')
    subparsers = top_parser.add_subparsers(help='sub-command help', dest='action')

    subparser = subparsers.add_parser('serve', help='Starts the main loop that continually checks email and responds to commands.')

    subparser = subparsers.add_parser('test-email', help='Sends a test email to the whitelist.')

    subparser = subparsers.add_parser('install', help='Installs the script.')
    subparser.add_argument('--venv-dir', default='')
    subparser.add_argument('--python-version', default='3.10')
    subparser.add_argument('--supervisor-dir', default='/etc/supervisor/conf.d/')
    subparser.add_argument('--log-path', default='/var/log/emailbot.log')
    subparser.add_argument('--dryrun', default=False, action='store_true', help='If given, no changes will be made.')

    args = top_parser.parse_args()
    action = args.action.replace('-', '_')

    bot = EmailBot()
    params = args.__dict__.copy()
    del params['action']
    getattr(bot, 'action_%s' % action)(**params)
