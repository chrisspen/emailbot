#!/bin/bash
set -e

ROLE=${1:-local}
VIRTUAL_ENV=${2:-.env}

cd "$(dirname "$0")"

echo "[$(date)] Removing existing virtualenv if it exists."
[ -d .env ] && rm -Rf .env

echo "[$(date)] Creating virtual environment with Python 3.10 (requires python3.10-venv package on Ubuntu)"
python3.10 -m venv .env

echo "[$(date)] Activating virtual environment."
. .env/bin/activate

echo "[$(date)] Upgrading pip and setuptools."
pip install -U pip setuptools

echo "[$(date)] Installing pip requirements."
pip install -r requirements.txt requirements-dev.txt
